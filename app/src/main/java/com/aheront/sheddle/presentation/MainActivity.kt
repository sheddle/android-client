package com.aheront.sheddle.presentation

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.aheront.sheddle.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}